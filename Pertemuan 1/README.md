![simplinnovation](https://1.bp.blogspot.com/-wStk0VZDfMk/YCC0GIRPrDI/AAAAAAAAAGc/1yj7IOUedvoeO1CuCxq7ETLW0FqXni6mwCLcBGAsYHQ/s320/logotext.png)

# __Tugas Pertemuan 1__

- Kelas: __Dasar Pemrograman__
- Program Studi: __Teknik Informatika__
- Perguruan Tinggi: __Universitas Nusa Putra__
- Dosen: __Alun Sujjada S.Kom, M.T.__
- Praktisi: __Lintang Wisesa Atissalam S.Si__

<hr/>

### Tugas Python Functions

1. Selesaikan tantangan [Codingbat Python](https://codingbat.com/python) untuk kategori [String 1](https://codingbat.com/python/String-1) (11 soal) dan [Logic 1](https://codingbat.com/python/Logic-1) (9 soal). Keseluruhan solusi berupa *return function*.
2. Publish jawaban tantangan ke repositori Anda di GitHub, GitLab atau BitBucket. Pastikan repo dapat diakses secara publik.
3. Submit link repo Anda ke Google Form berikut: [Submit di sini](https://forms.gle/FzQNYe6KBVGsz2zq5).

<hr/>

#### 🍔 Lintang Wisesa

<br>

<a href="mailto: lintangwisesa@ymail.com">
  <img align="left" style="margin-right:10px" alt="lintang ymail" width="22px" src="./../img/yahoo.png" />
</a>

<a href="https://web.facebook.com/lintangbagus/">
  <img align="left" style="margin-right:10px" alt="lintang facebook" width="22px" src="./../img/facebook.png" />
</a>

<a href="https://twitter.com/Lintang_Wisesa">
  <img style="margin-right:10px" align="left" alt="lintang twitter" width="23px" src="./../img/x.webp" />
</a>

<a href="https://www.youtube.com/user/lintangbagus">
  <img style="margin-right:10px" align="left" alt="lintang youtube" width="29px" src="./../img/youtube.png" />
</a>

<a href="https://www.linkedin.com/in/lintangwisesa/">
  <img style="margin-right:10px" align="left" alt="lintang linkedin" width="23px" src="./../img/linkedin.png" />
</a>

<a href="https://github.com/LintangWisesa">
  <img style="margin-right:10px" align="left" alt="lintang github" width="23px" src="./../img/github.png" />
</a>

<a href="https://gitlab.com/lintangwisesa26">
  <img style="margin-right:10px" align="left" alt="lintang gitlab" width="23px" src="./../img/gitlab.webp" />
</a>

<a href="https://www.hackster.io/lintangwisesa">
  <img style="margin-right:10px" align="left" alt="lintang hackster" width="23px" src="./../img/hackster.png" />
</a>

<a href="https://lintangwisesa.github.io/me/">
  <img style="margin-right:10px" align="left" alt="lintang bio" width="24px" src="./../img/lintang.png" />
</a>