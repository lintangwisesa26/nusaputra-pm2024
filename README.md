![simplinnovation](https://1.bp.blogspot.com/-wStk0VZDfMk/YCC0GIRPrDI/AAAAAAAAAGc/1yj7IOUedvoeO1CuCxq7ETLW0FqXni6mwCLcBGAsYHQ/s320/logotext.png)

# __Praktisi Mengajar 2024__

- Kelas: __Dasar Pemrograman__
- Program Studi: __Teknik Informatika__
- Perguruan Tinggi: __Universitas Nusa Putra__
- Dosen: __Alun Sujjada S.Kom, M.T.__
- Praktisi: __Lintang Wisesa Atissalam S.Si__

<hr/>

### 1. Python Functions

📚 Slide: [Slide Pertemuan 1](./Pertemuan%201/1%20Python%20Functions.pdf)
<br>
💻 Code: [Code Pertemuan 1](./Pertemuan%201/1%20Python%20Function.ipynb)
<br>
📝 Task: [Task Pertemuan 1](./Pertemuan%201/README.md)
<br>
📥 Submission: [Submit Pertemuan 1](https://forms.gle/1a6mrUR3pXAPsmPg7)
<br>
🎬 Video: [Video Pertemuan 1](https://www.youtube.com/watch?v=QnFv0unoqcs)

[![Video](https://img.youtube.com/vi/QnFv0unoqcs/0.jpg)](https://youtu.be/QnFv0unoqcs)

<hr/>

### 2. Python Modules

📚 Slide: [Slide Pertemuan 2](./Pertemuan%202/2%20Python%20Modules.pdf)
<br>
💻 Code:

- [Code Pertemuan 2 - Functions](./Pertemuan%202/2%20Python%20Functions.ipynb)
- [Code Pertemuan 2 - Modules](./Pertemuan%202/2%20Python%20Modules.ipynb)

<br>
🎬 Video: 

- [Video Pertemuan 2 - Functions](https://www.youtube.com/watch?v=a6EpBhuqbIo)
- [Video Pertemuan 2 - Module](https://www.youtube.com/watch?v=9JH2KLDIlHc)

[![Video](https://img.youtube.com/vi/a6EpBhuqbIo/0.jpg)](https://youtu.be/a6EpBhuqbIo)

[![Video](https://img.youtube.com/vi/9JH2KLDIlHc/0.jpg)](https://youtu.be/9JH2KLDIlHc)

<hr/>

### 3. Python Exception & File Handling

📚 Slide: 

- [Slide Pertemuan 3 - Exception Handling](./Pertemuan%203/3%20Exception%20Handling.pdf)
- [Slide Pertemuan 3 - File Handling](./Pertemuan%203/3%20File%20Handling.pdf)

<br>
💻 Code:

- [Code Pertemuan 3 - Exception Handling](./Pertemuan%203/3%20Exception%20Handling.ipynb)
- [Code Pertemuan 3 - File Handling](./Pertemuan%203/3%20File%20Handling.ipynb)

<br>
🎬 Video:

- [Video Pertemuan 3 - Exception Handling](https://www.youtube.com/watch?v=QNIW3i-yR8Y)
- [Video Pertemuan 3 - File Handling](https://www.youtube.com/watch?v=u9YebD4JwWQ)

[![Video](https://img.youtube.com/vi/QNIW3i-yR8Y/0.jpg)](https://youtu.be/QNIW3i-yR8Y)

[![Video](https://img.youtube.com/vi/u9YebD4JwWQ/0.jpg)](https://youtu.be/u9YebD4JwWQ)

<hr/>

### 4. Python Data Analysis

💻 Code: [Code Pertemuan 4 - Pandas Data Analysis](./Pertemuan%204/4%20Data%20Analysis.ipynb)
<br>
📝 Task:
- [Task Pertemuan 4: Soal 2 - 👨‍🎓 Kerja Kerja Kerja](https://github.com/LintangWisesa/Ujian_AnalyticsVisualization_JCDS07)
- [Task Pertemuan 4: Soal 2 - World Happiness](https://github.com/LintangWisesa/Ujian_AnalyticsVisualization_JCDS08)
<br>

📥 Submission: [Submit Pertemuan 4](https://forms.gle/1a6mrUR3pXAPsmPg7)
<br>
🎬 Video: [Video Pertemuan 4](https://www.youtube.com/watch?v=xUMglEAQuX0)

[![Video](https://img.youtube.com/vi/xUMglEAQuX0/0.jpg)](https://youtu.be/xUMglEAQuX0)

<hr/>

### 5. Python Data Visualization

💻 Code: [Code Pertemuan 5 - Matplotlib Data Visualization](./Pertemuan%205/0matplotlib.ipynb)
<br>
🎬 Video: [Video Pertemuan 5](https://www.youtube.com/watch?v=RgapNTmsMs0)

[![Video](https://img.youtube.com/vi/RgapNTmsMs0/0.jpg)](https://youtu.be/RgapNTmsMs0)

<hr/>

#### 🍔 Lintang Wisesa

<br>

<a href="mailto: lintangwisesa@ymail.com">
  <img align="left" style="margin-right:10px" alt="lintang ymail" width="22px" src="./img/yahoo.png" />
</a>

<a href="https://web.facebook.com/lintangbagus/">
  <img align="left" style="margin-right:10px" alt="lintang facebook" width="22px" src="./img/facebook.png" />
</a>

<a href="https://twitter.com/Lintang_Wisesa">
  <img style="margin-right:10px" align="left" alt="lintang twitter" width="23px" src="./img/x.webp" />
</a>

<a href="https://www.youtube.com/user/lintangbagus">
  <img style="margin-right:10px" align="left" alt="lintang youtube" width="29px" src="./img/youtube.png" />
</a>

<a href="https://www.linkedin.com/in/lintangwisesa/">
  <img style="margin-right:10px" align="left" alt="lintang linkedin" width="23px" src="./img/linkedin.png" />
</a>

<a href="https://github.com/LintangWisesa">
  <img style="margin-right:10px" align="left" alt="lintang github" width="23px" src="./img/github.png" />
</a>

<a href="https://gitlab.com/lintangwisesa26">
  <img style="margin-right:10px" align="left" alt="lintang gitlab" width="23px" src="./img/gitlab.webp" />
</a>

<a href="https://www.hackster.io/lintangwisesa">
  <img style="margin-right:10px" align="left" alt="lintang hackster" width="23px" src="./img/hackster.png" />
</a>

<a href="https://lintangwisesa.github.io/me/">
  <img style="margin-right:10px" align="left" alt="lintang bio" width="24px" src="./img/lintang.png" />
</a>